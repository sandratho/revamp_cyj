<?php
   include("includes/db.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Insert Product</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/bootstrap-icons.css" >

</head>
<body class="bg-light">
    <!-- Start Breadcrumbs -->
    <div class="container">
        <div class="col-12">
            <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Insert Product</li>
                </ol>
            </nav>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <div class="py-5 text-center">
      <h2>Upload A New Product</h2>
      <p class="lead">Below you can add new products. Be sure to chose the right category.</p>
    </div>


 


    <!-- Begin Main -->
    <div class="row">
        <div class="col-3"></div>
        <div class="col">
        <?php
    if (isset($_GET["error"])){
        if($_GET["error"] == "stmtfailed"){
            echo '<span style="color:#FF0000;text-align:center;">Something went wrong, try again</span>';
        } else if($_GET["error"] == "none") {
            echo '<span style="color:#32CD32;text-align:center; font-weight:bold;">Upload successfull!</span>';
            }
    }

    ?>
            <form class="form-horizontal" method="post" action="includes/insert_product.inc.php" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label">Product Title</label>
                    <input name="product_title" type="text" class="form-control" required>
                </div>
                <div class="form-group">
                    <label class="control-label">Product Category</label>
                    <select name="p_cat_id" class="form-control" required>
                        <option> Select a Product Category</option>
                        <?php
                            $sql = "SELECT * FROM product_categories";
                            $result = mysqli_query($conn, $sql);

                            while (false != $row = mysqli_fetch_array($result)) {
                                $p_cat_id = $row["p_cat_id"];
                                $p_cat_title = $row["p_cat_title"];

                                echo "<option value='$p_cat_id'> $p_cat_title </option>";
                            }
                        ?>      
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Category</label>
                    <select name="cat_id" class="form-control" >
                        <option> Select a Category</option>
                        <?php
                            $sql = "SELECT * FROM categories";
                            $result = mysqli_query($conn, $sql);

                            while (false != $row = mysqli_fetch_array($result)) {
                                $cat_id = $row["cat_id"];
                                $cat_title = $row["cat_title"];
                                echo "<option value='$cat_id'> $cat_title </option>";
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">                   
                    <label class="controllabel">Product Image</label>
                    <input name="product_image" type="file" class="form-control" required>
                </div>
                <div class="form-group">                   
                    <label class="control-label">Product Price</label>
                    <input name="product_price" type="text" class="form-control" required>
                </div>
                <div class="form-group">
                <label class="control-label">Product Keywords</label>
                    <input name="product_keywords" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label class="control-label">Product Description</label>
                    <textarea name="product_desc" cols="19" rows="6" class="form-control"></textarea>
                    <p></p>
                </div>
                <div class="form-group text-center">
                    <label class="control-label"> </label>
                    <input name="submit" value="Insert Product" type="submit" class="btn btn-primary form-control">
                </div>  
            </form>
        </div>
        <div class="col-3"></div>
    </div>
    <!-- End Main -->

    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>

