<?php
   include("../includes/db.php");
   include("../includes/functions.php");
?>

<?php
if(isset($_POST["submit"])){
    $p_cat_id = $_POST["p_cat_id"];
    $cat_id = $_POST["cat_id"];
    $product_title = $_POST["product_title"];
    $product_price = $_POST["product_price"];
    $product_keywords = $_POST["product_keywords"];
    $product_desc = $_POST["product_desc"];
    $product_image = $_FILES["product_image"]["name"];
    $temp_name = $_FILES["product_image"]["tmp_name"];
    move_uploaded_file($temp_name, "../product_images/$product_image");
    
    createProduct($conn, $p_cat_id, $cat_id, $product_title, $product_image, $product_price, $product_keywords, $product_desc);

}
else {
    header("location: ../insert_product.php");
}