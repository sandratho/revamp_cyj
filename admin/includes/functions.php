<?php

function createProduct($conn, $p_cat_id, $cat_id, $product_title, $product_image, $product_price, $product_keywords, $product_desc){

    $sql = "INSERT INTO products 
    (p_cat_id, cat_id, date, product_title, product_image, product_price, product_keywords, product_desc) VALUES (?,?,?,?,?,?,?,?);";
    $date = date('Y-m-d H:i:s', time());
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)){
        header("location: ../insert_product.php?error=stmtfailed");
        exit();
    }


    mysqli_stmt_bind_param($stmt, "iissssss",  $p_cat_id, $cat_id, $date, $product_title, $product_image, $product_price, $product_keywords, $product_desc);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    //TODO
    header("location: ../insert_product.php?error=none");
    exit();
  }

