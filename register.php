<?php
    include("includes/header.php");
?>

        <!-- Start Breadcrumbs -->
        <div class="container pt-3">
            <div class="col-md-12">
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Account</li>
                        <li class="breadcrumb-item active" aria-current="page">Register</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!-- End Breadcrumbs -->

        <!-- Start Sign Up -->
        <div class="container">
            <div class="col-12">
                <div class="text-center">
                    <hr class="my-4">
                    <h2>Sign Up</h2>
                    <?php
                    //GET to check visible data in URL
                    if (isset($_GET["error"])){
                        if($_GET["error"] == "invalidusername"){
                            echo '<span style="color:#FF0000;text-align:center;">Choose an alphanumeric username</span>';
                        }
                        else if($_GET["error"] == "passwordsdontmatch"){
                            echo '<span style="color:#FF0000;text-align:center;font-weight: bold;">Your Passwords Do Not Match</span>';
                        }
                        else if($_GET["error"] == "stmtfailed"){
                            echo '<span style="color:#FF0000;text-align:center;">Something went wrong. Please try again.</span>';
                        }
                        else if($_GET["error"] == "usernametaken"){
                            echo '<span style="color:#FF0000;text-align:center;">Username Taken. Try Another.</span>';

                        }
                        else if($_GET["error"] == "none"){
                        echo '<span style="color:#32CD32;text-align:center; font-weight:bold;">Sign Up - Successfull!</span>';
                        }
                    }
                ?>  
                </div>
               
            </div>
            <hr class="my-4">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <form class="g-3 needs-validation" action="register.php" method="post" enctype="multipart/form-data" novalidate>
                        <div class="row g-3">
                            <div class="col-sm-6">
                            <label for="firstName" class="form-label">Vorname</label>
                            <input type="text" class="form-control" name="firstname" required>
                            <div class="invalid-feedback">
                                Bitte gib einen Vornamen an
                            </div>
                            <div class="valid-feedback">
                                Sieht gut aus <i class="bi bi-hand-thumbs-up"></i>
                            </div>
                        </div>
            
                        <div class="col-sm-6">
                            <label for="lastName" class="form-label">Nachname</label> 
                            <input type="text" class="form-control" name="lastname" required>
                            <div class="invalid-feedback">
                                Bitte gib einen Nachnamen an
                            </div>
                            <div class="valid-feedback">
                                Sieht gut aus <i class="bi bi-hand-thumbs-up"></i>
                            </div>
                        </div>

                        <div class="col-6">
                            <label for="username" class="form-label">Benutzername</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                <i class="bi bi-person-circle"></i>
                                </span>
                                <input type="text" class="form-control" name="username" required>
                                <div class="invalid-feedback">
                                    Bitte gib einen Benutzernamen an
                                </div>
                                <div class="valid-feedback">
                                    Sieht gut aus <i class="bi bi-hand-thumbs-up"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">                
                            <label for="formFile" class="form-label">Profilbild</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                <i class="bi bi-person-circle"></i>
                                </span>
                                <input class="form-control" type="file" id="formFile" name="userfile">                                
                                <div class="valid-feedback">
                                    Sieht gut aus <i class="bi bi-emoji-smile"></i>
                                </div>
                            </div>
                        </div>



                        <div class="col-12">
                            <label for="password" class="form-label">Password</label>
                            <div class="input-group has-validation">
                            <span class="input-group-text">
                                <i class="bi bi-lock-fill"></i>
                            </span>
                            <input type="password" class="form-control" name="pwd" required>
                                <div class="invalid-feedback">
                                    Bitte gib dein Passwort ein
                                </div>                                              
                            </div>
                        </div>

                        <div class="col-12">
                        <label for="password" class="form-label">Repeat Password</label>
                        <div class="input-group has-validation">
                            <span class="input-group-text">
                                <i class="bi bi-lock-fill"></i>
                            </span>
                            <input type="password" class="form-control" name="pwdrepeat" id="pwdrepeat" placeholder="" required>
                            <div class="invalid-feedback">
                                Bitte gib dein Passwort ein
                            </div>                            
                        </div>
                        </div>

                    <div class="col-12">
                        <label for="telnumber" class="form-label">Cell Number</label>
                        <div class="input-group has-validation">
                        <span class="input-group-text">
                            <i class="bi bi-telephone-fill"></i>
                        </span>
                        <input type="text" class="form-control" name="telnumber">
                            <div class="valid-feedback">
                                Nicht verpflichtend
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <label for="telnumber" class="form-label">E-Mail</label>
                        <div class="input-group">
                            <span class="input-group-text">
                                <i class="bi bi-envelope-fill"></i>
                            </span>
                            <input type="text" class="form-control" name="email" required>
                            <div class="invalid-feedback">
                                Bitte gib eine gültige Mail-Adresse ein
                            </div>
                            <div class="valid-feedback">
                                Sieht gut aus <i class="bi bi-hand-thumbs-up"></i>
                            </div>
                        </div>
                    </div>  
                    
                    <div class="col-12">
                        <label for="telnumber" class="form-label">Adresse</label>
                        <div class="input-group">
                            <span class="input-group-text">
                                <i class="bi bi-geo-alt-fill"></i>
                            </span>
                            <input type="text" class="form-control" name="adress" required>
                            <div class="invalid-feedback">
                                Bitte gib deine Adresse an
                            </div>
                            <div class="valid-feedback">
                                Sieht gut aus <i class="bi bi-hand-thumbs-up"></i>
                            </div>
                        </div>
                    </div>                      

                    <div class="col-md-5">
                        <label for="country" class="form-label">Land</label>
                        <input type="text" class="form-control" placeholder="Österreich" name="country" required>
                        <div class="invalid-feedback">
                                Bitte gib dein Land an
                        </div> 
                        <div class="valid-feedback">
                            Sieht gut aus <i class="bi bi-hand-thumbs-up"></i>
                        </div>            
                    </div>

                    <div class="col-md-4">
                        <label for="state" class="form-label">Stadt</label>
                        <input type="text" class="form-control" name="state" id="state" placeholder="Wien" required>
                        <div class="invalid-feedback">
                                Bitte gib deine Stadt an
                        </div>
                        <div class="valid-feedback">
                            Sieht gut aus <i class="bi bi-hand-thumbs-up"></i>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <label for="zip" class="form-label">PLZ</label>
                        <input type="text" class="form-control" name="zip" id="zip" placeholder="1220" required>
                        <div class="invalid-feedback">
                                Bitte gib deine PLZ an
                        </div>
                        <div class="valid-feedback">
                            Sieht gut aus <i class="bi bi-hand-thumbs-up"></i>
                        </div>
                    </div>
                    <hr class="my-4">
                    <button class="btn btn-success btn-lg" type="submit" name="submit">Weiter</button>
                </form>  
                <div class="col-2"> </div>
            </div>            
        </div>
        <!-- End Sign Up -->



        <?php
            include("includes/footer.php");
        ?>

        <script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/validation.js"></script>
    </body>
</html>

<?php

if (isset($_POST["submit"])){
    $firstname = $_POST["firstname"];
    $lastname = $_POST["lastname"];
    $username = $_POST["username"];
    $pwd = $_POST["pwd"];
    $pwdRepeat = $_POST["pwdrepeat"];
    $telnumber = $_POST["telnumber"];
    $email = $_POST["email"];    
    $adress = $_POST["adress"];
    $country = $_POST["country"];
    $state = $_POST["state"];
    $zip = $_POST["zip"];
    $user_ip = getIpUser();

    $userfile = $_FILES["userfile"]["name"];
    $temp_name = $_FILES["userfile"]["tmp_name"];
    move_uploaded_file($temp_name, "customer/customer_images/$userfile");

    //create user
    $sql = "INSERT INTO users(username, firstname, lastname, email, pwd, telnumber, adress, country, state, zip, profilepicture, user_ip) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)){
        echo "Something went wrong, try again later."; //show error message if statement failed
        exit();
    }

    $hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);

    mysqli_stmt_bind_param($stmt, "ssssssssssss", $username, $firstname, $lastname, $email, $hashedPwd, $telnumber, $adress, $country, $state, $zip, $userfile, $user_ip);
    mysqli_stmt_execute($stmt);
  

    $sel_cart = "select * from cart where ip_add='$user_ip'";
    $run_cart = mysqli_query($conn,$sel_cart);    
    $check_cart = mysqli_num_rows($run_cart);
    
    if($check_cart>0){   
        // If there are items in cart
        $_SESSION['customer_email']=$email;
        echo "<script>alert('Registration successfull!')</script>";
        echo "<script>window.open('checkout.php','_self')</script>";
        
    }else{
        
        //If 0 items in cart
        $_SESSION['customer_email']=$email;
        echo "<script>alert('Registration successfull!')</script>";
        echo "<script>window.open('index.php','_self')</script>";
    }
    mysqli_stmt_close($stmt);

}