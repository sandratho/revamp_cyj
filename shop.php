<?php
    $active="shop";
    include("includes/header.php");
?>
 
        <div class="container pt-3">
            <!-- Start breadcrumbs -->
            <div class="col-md-12">
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Shop</li>
                    </ol>
                </nav>
            </div>
            <!-- End Breadcrumb -->

            
            <div class="row">
                <!-- Start SideBar -->
                <div class="col-3">
                    <?php
                    include("includes/sidebar.php");
                    ?>
                </div>
                <!-- End sidebar -->

                <!-- Start Main Shop -->
                <div class="col-9">
                    <?php
                        if(!isset($_GET['p_cat'])){
                            echo " 
                                <div class='shadow'> 
                                    <h3 class='text-center '> Store </h3>
                                    <div class='alert alert-light' role='alert'>
                                        Wähle hier dein Journal aus. In den Details siehst du noch deine gewählten Optionen als Übersicht und schon geht's los. In einigen Tagen erhältst du dein Journal bequem zu dir nach Hause geliefert.
                                    </div>
                                </div>";
                        }
                    ?>

                   
                    <div class="row row-cols-1 row-cols-md-3 g-4">
                        <?php
                                if(!isset($_GET["p_cat"])){
                                $sql = "SELECT * FROM products ORDER BY 1";
                                $result=mysqli_query($conn, $sql);
                                while (false != $row = mysqli_fetch_array($result)) {
                                    $pro_id = $row["product_id"];
                                    $product_title = $row["product_title"];
                                    $product_price = $row["product_price"];
                                    $product_image = $row["product_image"];
                                    
                                    echo "
                                        <div class='col'>
                                            <div class='card'>
                                                <a href='details.php?pro_id=$pro_id'>
                                                    <img src='admin/product_images/$product_image' class='card-img-top' alt='product'>
                                                </a>
                                                <div class='card-body'>
                                                    <h5 class='card-title text-center'>$product_title</h5>
                                                </div>
                                                <ul class='list-group list-group-flush'>
                                                    <li class='list-group-item'> <p class='text-end'> $product_price €</p></li>
                                                </ul>
                                                <div class='card-body text-center'>
                                                    <a href='details.php?pro_id=$pro_id'> <button type='button' class='btn btn-outline-secondary btn-sm'>View Details</button></a>
                                                    <a href='details.php?pro_id=$pro_id'> <p></p>
                                                        <button type='button' class='btn btn-secondary btn-sm'>
                                                            <i class='bi bi-cart'></i>
                                                            Add To Cart
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>";
                                }  
                            }                       
                        ?>                                    
                    </div>
                    
                    <?php
                    
                        getProductCategory();
                    ?>
                </div>
            </div>
        </div>     
        <!-- End Shop -->

        <?php
            include("includes/footer.php");
        ?>

        <script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
    </body>
</html>