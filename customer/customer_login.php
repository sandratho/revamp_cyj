<div class="row">
    <div class="col-3"></div>
    <div class="col">
        <main class="text-center">
            <form action="checkout.php" method="post">
                <h1 class="h2 mb-3 fw-normal">Anmelden</h1>
                <div class="text-muted">Noch kein Konto? <a href="register.php"> Hier kannst du dich registrieren!</a></div>
                <p></p>
                <div class="form-floating">
                    <input type="text" name="email" class="form-control" placeholder="name@example.com" required>
                        <label for="floatingInput">Email address</label>
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control" placeholder="Password" name="pwd" required>
                        <label for="floatingPassword">Password</label>
                </div>
                </br>
                <button class="btn btn-lg btn-success" type="submit" name="submit">
                    Log in
                </button>
                <p class="mt-5 mb-3 text-muted">&copy; 2022 CYJ, Inc.</p>
            </form>
        </main>
    </div>
    <div class="col-3"></div>
</div>

<?php
if(isset($_POST["submit"])){
    $email = $_POST["email"];
    $pwd = $_POST["pwd"];
    $get_ip = getIpUser();

    $userExists = usernameExists($conn, $email, $email);
    if ($userExists === false){
        echo "Wrong credentials, try again.";
        exit();
    }

    $pwdHashed = $userExists["pwd"]; //choose from associative array
    $checkPwd = password_verify($pwd, $pwdHashed);

    if ($checkPwd === false){ //show error message if wrong login
       echo "Wrong credentials, try again!";
       exit;
    }
    else if ($checkPwd === true) { //redirect to account page after login
        $_SESSION['customer_email']=$userExists["email"];
        echo "<script> window.open('customer/my_account.php?my_orders', '_self');</script>";
    }

    // $sql = "SELECT * FROM cart WHERE ip_add=$get_ip";
    // $result = mysqli_query($conn, $sql);
    // $check_cart = mysqli_num_rows($result);

}

