
<div class="container">
    <div class="text-center">
        <h1>Orders</h1>
        <p class="text-muted">All your Orders in one Place</p>
    </div>
    <table class="table table-striped">
        <thead>
            <th scope="col"> </th>
            <th scope="col">Bestelldatum</th>
            <th scope="col">Betrag</th>
            <th scope="col">Rechnungsnummer</th>
            <th scope="col">Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $customer_session = $_SESSION["customer_email"];
                $get_customer = "SELECT * FROM users WHERE email='$customer_session'";
                $run_customer = mysqli_query($conn, $get_customer);
                $row_customer = mysqli_fetch_array($run_customer);
                $customer_id = $row_customer["user_id"];

                $get_orders = "SELECT * FROM orders WHERE customer_id='$customer_id'";
                $run_orders = mysqli_query($conn, $get_orders);
                $row_orders = mysqli_fetch_array($run_orders);
                $i=0;
                while($row_orders = mysqli_fetch_array($run_orders)){
                    $order_id = $row_orders["order_id"];
                    $order_date = $row_orders["order_date"];
                    $order_due = $row_orders["due_amount"];
                    $order_invoice = $row_orders["invoice_no"];
                    $order_status = $row_orders["order_status"];
                    $i++;
                    if($order_status == "pending"){
                        $order_status = "Pay now";
                    }else{
                        $order_status = "Paid";
                    }       
            ?>
            <tr>
                <th scope="row"><?php echo $i; ?></th>
                <td><?php echo $order_date; ?></td>
                <td><?php echo $order_due; ?></td>
                <td><?php echo $order_invoice; ?></td>
                <td>
                    <a href="confirm.php?order_id=<?php echo $order_id; ?>" target="_blank">
                        <span class="badge rounded-pill text-bg-primary"><?php echo $order_status; ?></span>
                    </a>
                </td>
            </tr>                
            <!-- <span class="badge rounded-pill text-bg-warning">
                PAY NOW
            </span> -->     
        <?php
                }
        ?>
        </tbody>
    </table>
   
</div>