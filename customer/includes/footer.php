<div class="container" id="footer">
  <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
    <p class="col-md-4 mb-0 text-muted">&copy; 2022 CYJ, Inc</p>

    <a href="index.php" class="col-md-4 d-flex align-items-center justify-content-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
        <img src="../admin/admin_images/logo/logo_long.png"  width="260" height="50" alt=logo>
    </a>

    <ul class="nav col-md-4 justify-content-end list-unstyled d-flex">
      <li class="ms-3"><a class="text-muted" href="#"><i class="bi bi-twitter"></i></a></li>
      <li class="ms-3"><a class="text-muted" href="#"><i class="bi bi-facebook"></i></a></li>
      <li class="ms-3"><a class="text-muted" href="#"><i class="bi bi-instagram"></i></a></li>
    </ul>
  </footer>
</div>


