<div class="card shadow-sm p-3 mb-5 bg-body" style="width: 14rem;">
  <ul class="list-group list-group-flush">
  <?php 
    
    $customer_session = $_SESSION["customer_email"];
    $get_customer = "SELECT * FROM users WHERE email='$customer_session'";
    $run_customer = mysqli_query($conn, $get_customer);
    $row_customer = mysqli_fetch_array($run_customer);
    $customer_image = $row_customer["profilepicture"];
    $customer_name = $row_customer["firstname"];
    if(!isset($_SESSION["customer_email"])){
    }
    else{
      echo "
            <li class='list-group-item text-center'>
              <img src='customer_images/$customer_image' width='150' height='150'>
              <p>Hallo $customer_name !</p>
            </li>
      ";
    }
  ?>
    <li class="list-group-item <?php if(isset($_GET['my_orders'])){ echo "active";} ?>">
      <a href="my_account.php?my_orders" class="link-dark text-decoration-none">
        <i class="bi bi-bag-fill"></i> My Orders
      </a>
    </li>
    <li class="list-group-item <?php if(isset($_GET['edit_account'])){ echo "active";} ?>">
      <a href="my_account.php?edit_account" class="link-dark text-decoration-none" >
        <i class="bi bi-pencil-fill"></i> Edit Account
      </a>
    </li>
    <li class="list-group-item <?php if(isset($_GET['change_pwd'])){ echo "active";} ?>">
      <a href="my_account.php?change_pwd" class="link-dark text-decoration-none" >
        <i class="bi bi-key-fill"></i> Change Password
      </a>
    </li>
    <li class="list-group-item <?php if(isset($_GET['logout'])){ echo "active";} ?>">
      <a href="my_account.php?logout" class="link-dark text-decoration-none" >
      <i class="bi bi-box-arrow-right"></i> Logout
      </a>
    </li>
    <li class="list-group-item <?php if(isset($_GET['delete_acc'])){ echo "active";} ?>">
      <a href="my_account.php?delete_acc" class="link-dark text-decoration-none" >
      <i class="bi bi-trash3-fill"></i> Delete Account
      </a>
    </li>
  </ul>
</div>