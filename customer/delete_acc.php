<div class="container">
    <div class="text-center">
        <h1>Delete Account</h1>
        <p class="text-muted">Do you really want to delete your Account?</p>
        <form action="" method="post">
            <button class="btn btn-primary" type="submit" name="no">
                <i class="bi bi-caret-left-fill"></i>
                No, go back
            </button>
            <button class="btn btn-outline-danger"  type="submit" name="yes">       
                Yes, Delete
                <i class="bi bi-trash3-fill"></i>
            </button>            
        </form>
    </div>
</div>
<?php 
$c_email = $_SESSION['customer_email'];

if(isset($_POST['yes'])){
    $delete_customer = "DELETE FROM users WHERE email='$c_email'";
    $run_delete_customer = mysqli_query($conn,$delete_customer);
    
    if($run_delete_customer){
        session_destroy();
        echo "<script>alert('Konto gelöscht.')</script>";
        echo "<script>window.open('../index.php','_self')</script>";
    }   
}

if(isset($_POST['no'])){ 
    echo "<script>window.open('my_account.php?my_orders','_self')</script>";  
}

?>