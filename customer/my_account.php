<?php
   session_start();

   if(!isset($_SESSION['customer_email'])){       
       echo "<script>window.open('../checkout.php','_self')</script>";       
   }else{   
    include("includes/db.php");
    include("functions/functions.php");   
   }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Create Your Journal</title>
        <link rel="stylesheet" href="styles/bootstrap.css">
        <link rel="stylesheet" href="styles/styles.css">
        <link rel="stylesheet" href="styles/bootstrap-icons.css" >
    </head>
    <body>
        <!-- Start Header -->
        <div id="header">
            <nav class="navbar navbar-dark navbar-expand bg-dark  py-0">
                <div class="container-fluid">
                <a class="navbar-brand" href="../index.php">
                    <img src="../admin/admin_images/logo/logo_only.png" alt="logo" width="36" height="36">
                </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
                        <div class="navbar-nav">
                            <a class="nav-link <?php if($active=='home') echo 'active'; ?>" href="../index.php">Home</a>
                            <a class="nav-link <?php if($active=='shop') echo 'active'; ?>" href="../shop.php">Shop</a>                                                   
                                <?php                                    
                                    if (!isset($_SESSION['customer_email'])){
                                        echo " <a class='nav-link' href='checkout.php'>Mein Konto </a>";
                                    }
                                    else{
                                        echo " <a class='nav-link' href='my_account.php?my_orders'>Mein Konto </a>";
                                    }                    
                                    ?>                           
                            <li class="nav-item dropdown">
                                <a class="nav-link  dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="bi bi-person-circle"></i> Profil
                                </a>
                                <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                                    <li><a class="dropdown-item" href="../register.php"><i class="bi bi-pencil-fill"></i> Registrieren</a></li>
                                    <li><a class="dropdown-item" href="my_account.php?my_orders"><i class="bi bi-bag-fill"></i> Bestellungen</a></li>
                                    <?php
                                        if(isset($_SESSION["customer_email"])){
                                            echo " <li><a class='dropdown-item' href='logout.php'><i class='bi bi-box-arrow-right'></i> Log Out</a></li>";
                                        }
                                        else{
                                            echo "<li><a class='dropdown-item' href='checkout.php'><i class='bi bi-box-arrow-in-left'></i> Login</a></li>";
                                        }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                            if(!isset($_SESSION['customer_email'])){
                                echo "<a class='nav-link mx-5'>Hallo </a>";
                            }
                            else{
                                echo "<a class='nav-link mx-5'> <i class='bi bi-brightness-alt-high-fill'> </i> Hallo ".$_SESSION["customer_email"]. " <i class='bi bi-brightness-alt-high-fill'></i> </a>"; 
                            }

                            ?>                              
                        </div>
                        <form class="d-flex " role="cart">
                            <a class="nav-link form-control " href="../cart.php">
                                <i class="bi bi-cart-fill"> </i><?php items(); ?> Artikel | Gesamtpreis: <?php total_price(); ?>
                            </a>                                  
                        </form>
                    </div>                   
                    
                    <!-- <form class="d-flex">   
                    <a class="nav-link active link-secondary" href="cart.php"><i class="bi bi-cart-fill"></i> Einkaufswagen</a>
                    </form> -->
                </div>
            </nav>
        </div> 
        <!-- End Header -->
        <!-- breadcrumbs -->
        <div class="container">
            <div class="col-12">
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../index.php">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Mein Konto</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!-- End breadcrumbs -->

        <!-- Main Content -->
        <div class="container">
            <div class="row">
                <!-- Start Sidepanel -->
                <div class="col-3">
                    <?php
                        include("includes/sidebar.php");
                    ?>
                </div>
                <!-- End Sidepanel -->
                <div class="col-9">
                    <?php
                    if(isset($_GET['my_orders'])){
                        include("my_orders.php");
                    }

                    else if (isset($_GET['edit_account'])){
                        include("edit_account.php");
                    }

                    else if (isset($_GET['change_pwd'])){
                        include("change_pwd.php");
                    }

                    else if (isset($_GET['delete_acc'])){
                        include("delete_acc.php");
                    }

                    else if (isset($_GET['logout'])){
                        include("logout.php");
                    }



                    ?>
                </div>
            </div>
        </div>
        <!-- End Sidepanel -->
        
        <?php
            include("includes/footer.php");

        ?>

        <script src="../js/jquery-3.6.0.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
    </body>
</html>
