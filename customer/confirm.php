<?php 

session_start();

if(!isset($_SESSION['customer_email'])){
    
    echo "<script>window.open('../checkout.php','_self')</script>";
    
}else{

include("includes/db.php");
include("functions/functions.php");
    
if(isset($_GET['order_id'])){
    
    $order_id = $_GET['order_id'];
    
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Create Your Journal</title>
        <link rel="stylesheet" href="styles/bootstrap.css">
        <link rel="stylesheet" href="styles/styles.css">
        <link rel="stylesheet" href="styles/bootstrap-icons.css" >
    </head>
    <body>
        <!-- Start Header -->
        <div id="header">
            <nav class="navbar navbar-dark navbar-expand bg-dark  py-0">
                <div class="container-fluid">
                <a class="navbar-brand" href="../index.php">
                    <img src="../images/logo/logo_only.png" alt="logo" width="36" height="36">
                </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
                        <div class="navbar-nav">
                            <a class="nav-link" href="../index.php">Home</a>
                            <a class="nav-link" href="../shop.php">Shop</a>                        
                            <a class="nav-link" href="../customer/my_account.php">Mein Konto</a>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="bi bi-person-circle"></i> Profil
                                </a>
                                <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                                    <li><a class="dropdown-item" href="register.php"><i class="bi bi-pencil-fill"></i> Registrieren</a></li>
                                    
                                    <li><a class="dropdown-item" href="customer/my_orders.php"><i class="bi bi-bag-fill"></i> Bestellungen</a></li>
                                    <li>
                                    <?php
                                        if(isset($_SESSION["customer_email"])){
                                            echo " <li><a class='dropdown-item' href='logout.php'><i class='bi bi-box-arrow-right'></i> Log Out</a></li>";
                                        }
                                        else{
                                            echo "<li><a class='dropdown-item' href='checkout.php'><i class='bi bi-box-arrow-in-left'></i> Login</a></li>";
                                        }
                                    ?>                             
                                </ul>
                            </li>      
                            <a class='nav-link mx-5'>
                            <?php
                            if(!isset($_SESSION['customer_email'])){
                                echo "<a class='nav-link mx-5'>Welcome </a>";
                            }
                            else{
                                echo "<a class='nav-link mx-5'> <i class='bi bi-brightness-alt-high-fill'> </i> Welcome ".$_SESSION["customer_email"]. " <i class='bi bi-brightness-alt-high-fill'></i> </a>"; 
                            }

                            ?>
                            </a>
                        </div>
                        <form class="d-flex" role="cart">
                            <a class="nav-link form-control" href="../cart.php">
                                <i class="bi bi-cart-fill"> </i><?php items(); ?> Artikel | Gesamtpreis: <?php total_price(); ?>
                            </a>                                  
                        </form>
                    </div>
                   
                </div>
            </nav>
        </div> 
        <!-- End Header -->

   
    <!-- Start Breadcrumbs -->
    <div class="container pt-3">
            <div class="col-md-12">
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../index.php">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Account</li>
                        <li class="breadcrumb-item active" aria-current="page">Logging</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!-- End Breadcrumbs -->
        <div class="container">
        <div class="row">
            <!-- Start SideBar -->
            <div class="col-3">
                <?php
                include("includes/sidebar.php");
                ?>
            </div>
            <!-- End sidebar -->

            <!-- Start Main -->
            <div class="col-9">
            <h1> Zahlungsbestätigung</h1>
                   
                   <form action="confirm.php?update_id=<?php echo $order_id; ?>" method="post" enctype="multipart/form-data"><!-- form Begin -->
                       
                       <div class="form-group">
                           
                         <label> Rechnungsnummer: </label>
                          
                          <input type="text" class="form-control" name="invoice_no" required>
                           
                       </div>
                       
                       <div class="form-group">
                           
                         <label> Betrag: </label>
                          
                          <input type="text" class="form-control" name="amount" required>
                           
                       </div>
                       
                       <div class="form-group">
                           
                         <label> Bank </label>
                          
                          <select name="payment_mode" class="form-control">
                              
                              <option> Bank Austria </option>
                              <option> N26 </option>
                              <option> Easy Bank </option>
                              <option> Intesa Sanpaolo </option>
                              <option> Western Union </option>
                              
                          </select>
                           
                       </div>

                       <div class="form-group">
                           
                         <label> IBAN </label>
                          
                          <input type="text" class="form-control" name="iban" required>
                           
                       </div>
                       
                       <div class="form-group">
                           
                         <label> BIC </label>
                          
                          <input type="text" class="form-control" name="bic" required>
                           
                       </div>
                       
                       <div class="form-group">
                           
                         <label> Zahlungsdatum: </label>
                          
                          <input type="text" class="form-control" name="date" required>
                           
                       </div>
                       
                       <div class="text-center">
                           
                           <button class="btn btn-primary btn-lg" type="submit" name="confirm_payment">
                               
                               <i class="fa fa-user-md"></i> Bestätige Zahlung
                               
                           </button>
                       </div>
                   </form>
                   <?php
                        if(isset($_POST['confirm_payment'])){
                            $update_id = $_GET["update_id"];
                            $invoice_no = $_POST["invoice_no"];
                            $amount = $_POST["amount"];
                            $payment_mode = $_POST["payment_mode"];
                            $iban = $_POST["iban"];
                            $bic = $_POST["bic"];
                            $date = $_POST["date"];
                            $complete = "Payed";

                            $insert_payment = "INSERT INTO payments(invoice_no, amount, payment_mode, iban, bic, payment_date) 
                            VALUES ('$invoice_no', '$amount', '$payment_mode', '$iban', '$bic', '$payment_date')";
                            $run_payment = mysqli_query($conn, $insert_payment);

                            $update_customer_order = "UPDATE orders SET order_status='$complete' WHERE order_id='$update_id'";
                            $run_customer_order = mysqli_query($conn, $update_customer_order);
                            

                            $update_pending_order = "UPDATE pending_orders SET order_status='$complete' WHERE order_id='$update_id'";
                            $run_pending_order = mysqli_query($conn, $update_pending_order);

                        
                            
                            if($run_pending_order){
                                echo "<script>alert('Danke, dass du dich für uns entschieden hast');</script>";
                                echo "<script>window.open('my_account.php?my_orders','_self')</script>";
                            }


                        }
                   ?>
            </div>
            <!-- Ende Main -->
        </div>
    </div>          
   <?php 
    
    include("includes/footer.php");
    
    ?>
    
    <script src="js/jquery-331.min.js"></script>
    <script src="js/bootstrap-337.min.js"></script>
    
    
</body>
</html>
<?php } ?>