
<div class="container">
    <div class="text-center">
        <h1>Passwort ändern</h1>
        <p class="text-muted">Bitte wähle ein sicheres Passwort <i class="bi bi-shield-lock-fill"></i> </p>
    </div>
</div>
<form action="" method="post">
    <div class="col-12">
        <label for="password" class="form-label">New Password</label>
        <div class="input-group has-validation">
            <span class="input-group-text"><i class="bi bi-lock-fill"></i></span>
            <input type="password" class="form-control" name="newpwd" required>                   
        </div>
    </div>
    <div class="col-12">
        <label for="password" class="form-label">Repeat New Password</label>
        <div class="input-group has-validation">
            <span class="input-group-text"><i class="bi bi-lock-fill"></i></span>
            <input type="password" class="form-control" name="pwdrepeat" required>                   
        </div>
    </div>
    <div class="text-center">
        <div class="d-grid gap-2 col-6 py-3 mx-auto">
        <button class="btn btn-primary" type="submit" name="submit">Ändern</button>
        </div>
    </div>
</form>

<?php

if(isset($_POST['submit'])){
    $c_email = $_SESSION['customer_email'];
    $newpwd = $_POST['newpwd'];
    $pwdrepeat = $_POST['pwdrepeat'];

    if ($newpwd == $pwdrepeat){
        $hashedPwd = password_hash($newpwd, PASSWORD_DEFAULT);
        $update_pwd = "UPDATE users SET pwd='$hashedPwd' WHERE email='$c_email'";
        $run_pwd = mysqli_query($conn,$update_pwd);

        echo "<div class='alert alert-success d-flex align-items-center' role='alert'>
                <i class='bi bi-check-circle-fill'></i>
                <div>
                    Passwort geändert.
                </div>
            </div>";
    }
    else{
        echo "
        <div class='alert alert-primary d-flex align-items-center' role='alert'>
            <i class='bi bi-exclamation-triangle-fill'></i> 
            <div>
                Passwörter stimmen nicht überein.
            </div>
        </div>";
    }
}