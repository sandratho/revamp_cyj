<?php 

$customer_session = $_SESSION['customer_email'];
$sql = "select * from users where email='$customer_session'";
$result = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($result);

$user_id = $row['user_id'];
$usernameID = $row['username'];
$firstname = $row['firstname'];
$lastname = $row['lastname'];
$email = $row['email'];
$telnumber = $row['telnumber'];
$adress = $row['adress'];
$country = $row['country'];
$state = $row['state'];
$zip = $row['zip'];
$profilepicture = $row['profilepicture'];

?>

<div class="container">
    <div class="row">
        <h1>Account</h1>
        <p class="text-muted">Edit your account information</p>
    </div>
    <!-- Start Form -->
    <form action="" method="post" enctype="multipart/form-data">

        <!-- Start Formgroup -->
        <fieldset disabled="disabled">
            <div class="row g-3 align-items-center py-3">  
                <div class="col-2">
                    <label for="username" class="col-form-label">Username</label>
                </div>
                <div class="col-auto">
                    <input type="text" name="usernameID" class="form-control" value="<?php echo $usernameID; ?>" required>
                </div>
                <div class="col-auto">
                    <span id="usernameHelpInline" class="form-text">
                    Der Benutzername lässt sich nicht mehr ändern. 
                    </span>
                </div>
            </div>
        </fieldset>

        <div class="row g-2 align-items-center py-3">
            <div class="col-2">
                <label for="firstname" class="col-form-label">Vorname</label>
            </div>
            <div class="col-auto">
                <input type="text" name="firstname" class="form-control" value="<?php echo $firstname; ?>" required>
            </div>
        </div>

        <div class="row g-2 align-items-center py-3">
            <div class="col-2">
                <label for="lastname" class="col-form-label">Nachname</label>
            </div>
            <div class="col-auto">
                <input type="text" name="lastname" class="form-control" value="<?php echo $lastname; ?>" required>
            </div>
        </div>

        <div class="row g-2 align-items-center py-3">
            <div class="col-2">
                <label for="email" class="col-form-label">E-Mail</label>
            </div>
            <div class="col-auto">
                <input type="text" name="email" class="form-control" value="<?php echo $email; ?>" required>
            </div>
        </div>

        <div class="row g-2 align-items-center py-3">
            <div class="col-2">
                <label for="telumber" class="col-form-label">Telefonnummer:</label>
            </div>
            <div class="col-auto">
                <input type="text" name="telnumber" class="form-control" value="<?php echo $telnumber; ?>" required>
            </div>
        </div>

        <div class="row g-2 align-items-center py-3">
            <div class="col-2">
                <label for="adress" class="col-form-label">Adresse:</label>
            </div>
            <div class="col-auto">
                <input type="text" name="adress" class="form-control" value="<?php echo $adress; ?>" required>
            </div>
        </div>

        <div class="row g-2 align-items-center py-3">
            <div class="col-2">
                <label for="country" class="col-form-label">Land:</label>
            </div>
            <div class="col-auto">
                <input type="text" name="country" class="form-control" value="<?php echo $country; ?>" required>
            </div>
        </div>

        <div class="row g-2 align-items-center py-3">
            <div class="col-2">
                <label for="state" class="col-form-label">Stadt:</label>
            </div>
            <div class="col-auto">
                <input type="text" name="state" class="form-control" value="<?php echo $state; ?>" required>
            </div>
        </div>

        <div class="row g-2 align-items-center py-3">
            <div class="col-2">
                <label for="zip" class="col-form-label">PLZ:</label>
            </div>
            <div class="col-auto">
                <input type="text" name="zip" class="form-control" value="<?php echo $zip; ?>" required>
            </div>
        </div>

        <div class="row g-2 align-items-center py-3">
            <div class="col-2">
                <label for="profilepicture" class="col-form-label">Profilbild</label>
            </div>
            <div class="col-auto">
                <input type="file" name="profilepicture" class="form-controlform-height-custom" value="<?php echo $profilepicture; ?>">
                <img class="img-responsive" src="customer_images/<?php echo $profilepicture; ?>" alt="Avatar" widht="100" height="100">
            </div>
        </div>
        <!-- Form Group Finished -->
    
        <div class="text-center">
            <div class="d-grid gap-1">
                <button class="btn btn-primary" type="submit" name="update">
                        Aktualisieren
                    <i class="bi bi-arrow-clockwise"></i>
                </button>
            </div>        
    </div>
    </form>
    <!-- form Finish -->
</div
<?php

if(isset($_POST['update'])){

    $update_id = $user_id;

    $usernameID = $_POST['usernameID'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $telnumber = $_POST['telnumber'];
    $adress = $_POST['adress'];
    $country = $_POST['country'];
    $state = $_POST['state'];
    $zip = $_POST['zip'];

    $profilepicture = $_FILES['profilepicture']['name'];    
    $profilepicture_tmp = $_FILES['profilepicture']['tmp_name'];  
    
    move_uploaded_file ($profilepicture_tmp,"customer_images/$profilepicture");
    
    $update_user= "UPDATE users SET firstname='$firstname', lastname='$lastname' , email='$email' , telnumber='$telnumber', adress='$adress', country='$country', state='$state', zip='$zip', profilepicture='$profilepicture' where user_id='$update_id'";
    $run_customer = mysqli_query($conn,$update_user);
    
    if($run_customer){
        
        echo "<script>alert('Deine Daten wurden aktualisiert. Bitte melde dich erneut an.')</script>";      
        echo "<script>window.open('logout.php','_self')</script>";
        
    }
    
}
