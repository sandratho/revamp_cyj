<?php
  $db = mysqli_connect('localhost', 'root', "", "journalstore");

//function to get IP from User: https://stackoverflow.com/questions/3003145/how-to-get-the-client-ip-address-in-php
function getIpUser(){
    switch(true){
        case(!empty($_SERVER['HTTP_X_REAL_IP'])) : return $_SERVER['HTTP_X_REAL_IP']; //IP of last request
        case(!empty($_SERVER['HTTP_CLIENT_IP'])) : return $_SERVER['HTTP_CLIENT_IP']; //IP if shared
        case(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) : return $_SERVER['HTTP_X_FORWARDED_FOR']; //IP from proxy

        default : return $_SERVER['REMOTE_ADDR']; //reliable, hard to spoof, contains real IP
    }
}

//function to add items to users cart identified by IP
function add_cart(){
    global $db;
    if(isset($_GET["add_cart"])){
        $ip_add = getIpUser();
        $p_id = $_GET["add_cart"];
        $product_qty = $_POST["product_qty"];
        $year = $_POST["year"];
        $month  = $_POST["month"];
        $weeks = $_POST["weeks"];
        $notes  = $_POST["notes"];

        $sqlCheck = "select * from cart where ip_add='$ip_add' AND p_id='$p_id'";
        $result = mysqli_query($db, $sqlCheck);

        if(mysqli_num_rows($result)>0){
            
            echo "<script>alert('This product has already added in cart')</script>";
            echo "<script>window.open('details.php?pro_id=$p_id','_self')</script>";
            
        }else{
            
            $query = "INSERT INTO cart (p_id,ip_add,qty,annual, monthly, weekly, notes) values ($p_id,'$ip_add',$product_qty,$year, $month, $weeks, $notes)";
            
            $run_query = mysqli_query($db,$query);
            
            echo "<script>window.open('details.php?pro_id=$p_id','_self')</script>";
            
        }
    }    
}



// function to fetch only 4 products to display on the homepage
  function getPro(){
    global $db;

    $sql = "SELECT * FROM products order by 1 DESC LIMIT 0,4";
    $result = mysqli_query($db, $sql);

    while (false != $row = mysqli_fetch_array($result)) {
        $product_id = $row["product_id"];
        $product_title = $row["product_title"];
        $product_price = $row["product_price"];
        $product_image = $row["product_image"];

        echo "
            <div class='col'>
                <div class='card'>                  
                    <img src='admin/product_images/$product_image' class='card-img-top' alt='Plan Color Cover'>
                    <div class='card-body'>
                        <h5 class='card-title text-center'>$product_title</h5>
                    </div>
                    <ul class='list-group list-group-flush'>
                        <li class='list-group-item'> <p class='text-end'> $product_price €</p></li>
                    </ul>
                    <div class=card-body text-center>
                        <a href='details.php?pro_id=$product_id'> <button type='button' class='btn btn-outline-secondary btn-sm'>View Details</button></a>
                        <a href='details.php?pro_id=$product_id'> 
                            <button type='button' class='btn btn-secondary btn-sm'>
                                <i class='bi bi-cart2'></i>
                                Add To Cart
                            </button>
                        </a>
                    </div>
                </div>
            </div>";
    }

  }

//   function to display all categories in the sidebar element
  function getCategory(){
      global $db;

      $sql = "SELECT * FROM product_categories";
      $result = mysqli_query($db, $sql);

   while (false != $row = mysqli_fetch_array($result)) {
      $p_cat_id = $row["p_cat_id"];
      $p_cat_title = $row["p_cat_title"];
      echo "     
      <li class='list-group-item'>
        <a href='shop.php?p_cat=$p_cat_id' class='link-secondary'>
          $p_cat_title
        </a>
      </li>";
   }
  }


//   function to fetch all products belonging the choosen category 
  function getProductCategory(){
    
    global $db;
    
    if(isset($_GET['p_cat'])){
        
        $p_cat_id = $_GET['p_cat'];
        
        $get_p_cat ="select * from product_categories where p_cat_id='$p_cat_id'";
        
        $run_p_cat = mysqli_query($db,$get_p_cat);
        
        $row_p_cat = mysqli_fetch_array($run_p_cat);
        
        $p_cat_title = $row_p_cat['p_cat_title'];
        
        $p_cat_desc = $row_p_cat['p_cat_desc'];
        
        $get_products ="select * from products where p_cat_id='$p_cat_id'";
        
        $run_products = mysqli_query($db,$get_products);
        
        $count = mysqli_num_rows($run_products);
        
        if($count==0){
            
            echo "            
                <div class='box'>                
                    <h1> More Products Comming Soon! </h1>                
                </div>           
            ";
            
        }else{            
            echo "            
                <div class='col-12'>                
                    <h1> $p_cat_title </h1>                    
                    <p> $p_cat_desc </p>                
                </div>
                <div class='row row-cols-1 row-cols-md-3 g-4'>            
                           
            ";            
        }
        
        while (false != $row = mysqli_fetch_array($run_products)) {
            $product_id = $row["product_id"];
            $product_title = $row["product_title"];
            $product_price = $row["product_price"];
            $product_image = $row["product_image"];
    
            echo "
                <div class='col'>
                    <div class='card'>                  
                        <img src='admin/product_images/$product_image' class='card-img-top' alt='Plan Color Cover'>
                        <div class='card-body'>
                            <h5 class='card-title'>$product_title</h5>
                        </div>
                        <ul class='list-group list-group-flush'>
                            <li class='list-group-item'> <p class='text-end'> $product_price €</p></li>
                        </ul>
                        <div class=card-body text-center>
                            <a href='details.php?pro_id=$product_id'> <button type='button' class='btn btn-outline-secondary btn-sm'>View Details</button></a>
                            <a href='details.php?pro_id=$product_id'> 
                                <button type='button' class='btn btn-secondary btn-sm'>
                                    <i class='bi bi-cart2'></i>
                                    Add To Cart
                                </button>
                            </a>
                        </div>
                    </div>
                </div>";
            
        }
        echo "</div>";
        
    }
    
}

//function to show number of items in cart
function items(){
    global $db;
    $ip_add = getIpUser();
    $sql = "SELECT * FROM cart WHERE ip_add='$ip_add'";
    $result = mysqli_query($db, $sql);
    $count = mysqli_num_rows($result);
    echo $count;
}

//function to estimate total price of items in cart
function total_price(){
    global $db;
    $ip_add = getIpUser();
    $total=0;
    $sql = "SELECT * FROM cart WHERE ip_add='$ip_add'";
    $result = mysqli_query($db, $sql);

    while (false != $row = mysqli_fetch_array($result)) {
        $pro_id = $row['p_id'];
        $pro_qty = $row['qty'];

        $getprice = "SELECT * FROM products WHERE product_id='$pro_id'";
        $runprice = mysqli_query($db, $getprice);

        while (false != $row = mysqli_fetch_array($runprice)) {
            $subtotal = $row["product_price"] * $pro_qty;
            $total += $subtotal; 
        }
     }
    echo "$total €";
}

function deleteProduct($product_id){
    global $db;
    $delete_product = "DELETE FROM cart WHERE p_id='$product_id'";  
    $rundelete = mysqli_query($db, $delete_product);    
}


// Sign Up and Login Functions Start //


//check if username has special chars
function invalidUsername ($username) { 
    if (!preg_match('/^[A-Za-z0-9]+$/', $username)) {
       $result = true;
   }
   else {
    $result = false;
   }
   return $result; 
}

//check if passwords match or output error
function pwdMatch($pwd, $pwdRepeat){
    if ($pwd !== $pwdRepeat){
        $result = true;
    }
    else{
        $result = false;
    }
    return $result;
}

//check if username exists or output error
function usernameExists ($conn, $username, $email){
    $sql = "SELECT * FROM users WHERE username = ? OR email = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)){
        echo "Something went wrong.";
        exit();
    }
    mysqli_stmt_bind_param($stmt, "ss", $username, $email);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($resultData)){ //returned as associative array
        return $row;
    }
    else{
        $result = false;
        return $result;
    }

    mysqli_stmt_close($stmt);
}

//function to create User
function createUser ($conn, $username, $firstname, $lastname, $email, $pwd, $telnumber, $adress, $country, $state, $zip, $profilepicture, $user_ip){
    $sql = "INSERT INTO users(username, firstname, lastname, email, pwd, telnumber, adress, country, state, zip, profilepicture, user_ip) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)){
        header("location: ../register.php?error=stmtfailed"); //show error message if statement failed
        exit();
    }

    $hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);

    mysqli_stmt_bind_param($stmt, "ssssssssssss", $username, $firstname, $lastname, $email, $hashedPwd, $telnumber, $adress, $country, $state, $zip, $profilepicture, $user_ip);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("location: ../register.php?error=none"); //show successfull sign up message
    session_start();
    $_SESSION['firstname'] = $firstname;
    exit();
}

//function to login User 
function loginUser($conn, $username, $pwd){
    $userExists = usernameExists($conn, $username, $username);

    if ($userExists === false){
        header("location: ../login.php?error=wronglogin");
        exit();
    }

    $pwdHashed = $userExists["pwd"]; //choose from associative array
    $firstname = $userExists["firstname"];
    $checkPwd = password_verify($pwd, $pwdHashed);

    if ($checkPwd === false){ //show error message if wrong login
        header("location ../login.php?error=wronglogin");
        exit();
    }
    else if ($checkPwd === true) { //redirect to account page after login
        $_SESSION["firstname"] = $firstname;
        $_SESSION["username"] = $userExists["username"];
        exit;
    }
}


//SQL ERROR CHECKING
    // if (!$result) {
    //     printf("Error: %s\n", mysqli_error($db));
    //     exit();
    // }







