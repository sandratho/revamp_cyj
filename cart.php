<?php
    include("includes/header.php");
?>
        <div class="container pt-3">
            <!-- Start Breadcrumbs -->
            <div class="col-12">
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Einkaufswagen</li>
                    </ol>
                </nav>
            </div>
            
            <!-- End Breadcrumbs -->

            <!-- Start Table Shopping Cart -->
            <div class="row">
                <div class="col-8">
                <div class="h4 pb-2 mb-4 text-success border-bottom border-success">
                    Dein Einkauf
                </div>                
                <div class="text-muted">
                    Es sind <?php items(); ?> Artikel in deinem Einkaufswagen
                </div>
                    <form action="cart.php" method="post" enctype="multipart/form-data">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Produkt</th>
                                    <th>Jahres Sicht</th>
                                    <th>Monats Sicht</th>
                                    <th>Wochen Sicht</th>
                                    <th>Leere Notizen</th>
                                    <th>Menge</th>
                                    <th>Löschen</th>
                                    <th>Preis</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $total = 0;
                                    $ip_add = getIpUser();
                                    $select_cart = "SELECT * FROM cart WHERE ip_add='$ip_add'";
                                    $run_cart = mysqli_query($conn, $select_cart);

                                    while (false != $row = mysqli_fetch_array($run_cart)) {
                                        $product_id = $row["p_id"];
                                        $product_quantity = $row["qty"];
                                        $product_annual= $row["annual"];
                                        $product_monthly = $row["monthly"]; 
                                        $product_weekly = $row["weekly"]; 

                                        $product_notes = $row["notes"]; 

                                        $getProducts = "SELECT * FROM products WHERE product_id=$product_id";
                                        $runProducts = mysqli_query($conn, $getProducts);

                                        while (false != $rowProducts = mysqli_fetch_array($runProducts)) { 
                                            $product_title = $rowProducts["product_title"]; 
                                            $product_image = $rowProducts["product_image"];
                                            $product_price = $rowProducts["product_price"];    
                                            $subtotal = $rowProducts["product_price"] * $product_quantity;   
                                            $total += $subtotal;                        
                                ?>
                                <tr>
                                    <td>
                                        <a href="details.php?pro_id=<?php echo $product_id; ?>">
                                            <img src="admin/product_images/<?php echo "$product_image"; ?>" alt="Product" width="130" height="130"></td>
                                        </a>
                                        <td>
                                        <?php 
                                        if ($product_annual == 1){
                                            echo "<i class='bi bi-check-circle-fill text-success'></i>"; 
                                        }
                                        else {
                                            echo "<i class='bi bi-x-circle-fill text-danger'></i>";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                    <?php 
                                        if ($product_monthly == 1){
                                            
                                            echo "<i class='bi bi-check-circle-fill text-success'></i>"; 
                                        }
                                        else {
                                            echo "<i class='bi bi-x-circle-fill text-danger'></i>";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                        if ($product_weekly == 1){
                                            echo "<i class='bi bi-check-circle-fill text-success'></i>"; 
                                        }
                                        else {
                                            echo "<i class='bi bi-x-circle-fill text-danger'></i>";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                        if ($product_notes == 1){
                                            echo "<i class='bi bi-check-circle-fill text-success'></i>"; 
                                        }
                                        else {
                                            echo "<i class='bi bi-x-circle-fill text-danger'></i>";
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $product_quantity;?></td>
                                    <td>
                                        <form action="" method="post">
                                            <button type="submit" class="btn btn-outline-danger" name="remove" value="<?php echo $product_id;?>">
                                                <i class="bi bi-trash3-fill"></i>                                                 
                                            </button>
                                        </form>
                                    </td>
                                    <td><?php echo $subtotal; ?></td>
                                </tr>                         

                                <?php } } 
                                 if (isset($_POST["remove"])){
                                    $value = $_POST["remove"];
                                    deleteProduct($value);
                                    // script to reload page with HTML DOM window local storage
                                    echo "                                        
                                        <script type='text/javascript'>   
                                            //anonymous function
                                            (() => {
                                                if (window.localStorage) {
                                    
                                                    // If there is no item as 'reload' in localstorage then create one &
                                                    // reload the page
                                                    if (!localStorage.getItem('reload')) {
                                                        localStorage['reload'] = true;
                                                        window.location.reload();
                                                    } else {
                                    
                                                        // If there exists a 'reload' item then clear the 'reload' item in
                                                        // local storage
                                                        localStorage.removeItem('reload');
                                                    }
                                                }
                                            })();
                                        </script>";
                                    }  
                                ?>
                                
                            </tbody>
                        </table>
                            <div class="d-flex justify-content-between">
                                <a href="shop.php"><button class="btn btn-outline-primary">
                                    <i class="bi bi-arrow-left-circle"></i>
                                    Weiter Einkaufen</button>
                                </a>
                                <a href="checkout.php"> <button type="button" class="btn btn-primary">
                                    Bezahlen
                                    <i class="bi bi-cart-check"></i>
                                </button> </a>
                            </div>
                    </form>
                </div>
                <!-- End Shopping Cart -->                
                

                <!-- Start Order Summary -->
                <div class="col-3 shadow p-3 mb-5 bg-body">
                    <p class="fs-3"> Bestellübersicht</p>
                    
                    <div class="row">
                        <div class="col-7">Zwischensumme</div>
                        <div class="col-5"> 
                            <strong> <?php echo $total;?>€</strong>
                        </div>
                    
                    <hr>
                    
                        <div class="col-7">Versand</div>
                        <div class="col-5">
                        <span class="badge rounded-pill text-bg-success">KOSTENLOS</span>

                        </div>
                    
                    <hr>
                    
                        <div class="col-7">20% MwSt.</div>
                        <div class="col-5">
                            <strong> 
                            <?php 
                                $VAT = $total*20/100;
                                $totalVAT = $total + ($total*5/100);
                                echo "$VAT €";
                            ?>
                            </strong>    
                        </div>
                    
                    <hr>
                    
                        <div class="col-7">Gesamt</div>
                        <div class="col-5"><strong><?php echo "$totalVAT €"; ?></strong></div>                           
                </div>
                <!-- End Order Summary -->

            </div> <!-- End Row -->
            

            
            
        </div>
        <?php
            include("includes/footer.php");

        ?>

        <script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
    </body>
</html>