<?php
    include("includes/header.php");
    include_once("functions/functions.php");
?>

        <!-- Start Breadcrumbs -->
        <div class="container pt-3">
            <div class="col-md-12">
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Account</li>
                        <li class="breadcrumb-item active" aria-current="page">Logging</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!-- End Breadcrumbs -->

    <div class="container">
        <div class="row">
            <!-- Start SideBar -->
            <div class="col-3">
                <?php
                include("includes/sidebar.php");
                ?>
            </div>
            <!-- End sidebar -->

            <!-- Start Main -->
            <div class="col-9">
                <?php
                    if(!isset($_SESSION['customer_email'])){
                        include("customer/customer_login.php");
                    }else{
                        include("payment_options.php");
                    }
                ?>
            </div>
            <!-- End Main -->
        </div>
    </div>
       


        <?php
            include("includes/footer.php");
        ?>

        <script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
    </body>
</html>