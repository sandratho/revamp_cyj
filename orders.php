<?php
    session_start();
    include_once("includes/db.php");
    include_once("functions/functions.php");

    if(isset($_GET["user_id"])){
        $user_id = $_GET["user_id"];
    }

    $ip_add = getIpUser();
    $status = "pending";
    $invoice_no = mt_rand();
    $select_cart = "SELECT * FROM cart WHERE ip_add='$ip_add'";
    $run_cart = mysqli_query($conn, $select_cart);
    while ($row= mysqli_fetch_array($run_cart)){
        $pro_id = $row["p_id"];
        $pro_qty = $row["qty"];
        $pro_annual = $row["annual"];
        $pro_monthly = $row["monthly"];
        $pro_weekly = $row["weekly"];
        $pro_notes = $row["notes"];

        $get_products = "SELECT * FROM products WHERE product_id=$pro_id";
        $run_products = mysqli_query($conn, $get_products);
        while($row_products= mysqli_fetch_array($run_products)){

            $sub_total = $row_products["product_price"]*$pro_qty;

            $insert_customer_order = "INSERT INTO orders (customer_id, due_amount, invoice_no, qty, annual, monthly, weekly, notes, order_date, order_status) 
            VALUES ('$user_id','$sub_total','$invoice_no','$pro_qty','$pro_annual','$pro_monthly','$pro_weekly', '$pro_notes', NOW(), '$status')";
            
            $run_customer_order = mysqli_query($conn, $insert_customer_order);
            
            $insert_pending_order = "INSERT INTO pending_orders (customer_id, invoice_id, product_id, qty, annual, monthly, weekly, notes, order_status) 
            VALUES ('$user_id','$invoice_no', $pro_id, '$pro_qty','$pro_annual','$pro_monthly','$pro_weekly', '$pro_notes', '$status')";
            
            $run_pending_order = mysqli_query($conn, $insert_pending_order);
            
            $delete_cart = "DELETE FROM cart WHERE ip_add='$ip_add'";
            
            $run_delete = mysqli_query($conn, $delete_cart);
            
            echo "<script>alert('Your Orders have been placed!');</script>";
            echo "<script>window.open('customer/my_account.php?my_orders', '_self');</script>";
        }
    }
?>