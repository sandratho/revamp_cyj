<?php
    session_start();
    include("includes/db.php");
    include("functions/functions.php");

    if(isset($_GET['pro_id'])){
        $product_id = $_GET['pro_id'];
        $sql = "SELECT * FROM products WHERE product_id='$product_id'";
        $result = mysqli_query($conn, $sql);
        $row = mysqli_fetch_array($result);
        $p_cat_id = $row['p_cat_id'];
        $product_title = $row['product_title'];
        $product_price = $row["product_price"];
        $product_desc = $row["product_desc"];
        $product_image = $row["product_image"];
        $get_p_cat = "SELECT * FROM product_categories WHERE p_cat_id='$p_cat_id'";
        $run_p_cat = mysqli_query($conn, $get_p_cat);
        $row = mysqli_fetch_array($run_p_cat);
        $p_cat_title = $row['p_cat_title'];
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Create Your Journal</title>
        <link rel="stylesheet" href="styles/bootstrap.css">
        <link rel="stylesheet" href="styles/styles.css">
        <link rel="stylesheet" href="styles/bootstrap-icons.css" >
    </head>
    <body>
        <!-- Start Header -->
        <div id="header">
            <nav class="navbar navbar-dark navbar-expand bg-dark  py-0">
                <div class="container-fluid">
                <a class="navbar-brand" href="index.php">
                    <img src="images/logo/logo_only.png" alt="logo" width="36" height="36">
                </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
                        <div class="navbar-nav">
                            <a class="nav-link <?php if($active=='home') echo 'active'; ?>" href="index.php">Home</a>
                            <a class="nav-link <?php if($active=='shop') echo 'active'; ?>" href="shop.php">Shop</a>                                                   
                                <?php                                    
                                    if (!isset($_SESSION['customer_email'])){
                                        echo " <a class='nav-link' href='checkout.php'>Mein Konto </a>";
                                    }
                                    else{
                                        echo " <a class='nav-link' href='customer/my_account.php?my_orders'>Mein Konto </a>";
                                    }                    
                                    ?>                           
                            <li class="nav-item dropdown">
                                <a class="nav-link  dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="bi bi-person-circle"></i> Profil
                                </a>
                                <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                                    <li><a class="dropdown-item" href="register.php"><i class="bi bi-pencil-fill"></i> Registrieren</a></li>
                                    <?php
                                        if(isset($_SESSION["customer_email"])){
                                            echo " <li><a class='dropdown-item' href='logout.php'><i class='bi bi-box-arrow-right'></i> Log Out</a></li>";
                                        }
                                        else{
                                            echo "<li><a class='dropdown-item' href='checkout.php'><i class='bi bi-box-arrow-in-left'></i> Login</a></li>";
                                        }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                            if(!isset($_SESSION['customer_email'])){
                                echo "<a class='nav-link mx-5'>Hallo </a>";
                            }
                            else{
                                echo "<a class='nav-link mx-5'> <i class='bi bi-brightness-alt-high-fill'> </i> Hallo ".$_SESSION["customer_email"]. " <i class='bi bi-brightness-alt-high-fill'></i> </a>"; 
                            }

                            ?>

                                                             
                        </div>
                        <form class="d-flex " role="cart">
                            <a class="nav-link form-control " href="cart.php">
                                <i class="bi bi-cart-fill"> </i><?php items(); ?> Artikel | Gesamtpreis: <?php total_price(); ?>
                            </a>                                  
                        </form>
                    </div>                   
                    
                    <!-- <form class="d-flex">   
                    <a class="nav-link active link-secondary" href="cart.php"><i class="bi bi-cart-fill"></i> Einkaufswagen</a>
                    </form> -->
                </div>
            </nav>
        </div> 
        <!-- End Header -->