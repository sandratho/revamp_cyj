-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 08. Jun 2022 um 17:35
-- Server-Version: 10.4.24-MariaDB
-- PHP-Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `journalstore`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cart`
--

CREATE TABLE `cart` (
  `p_id` int(10) NOT NULL,
  `ip_add` varchar(255) NOT NULL,
  `qty` int(10) NOT NULL,
  `annual` int(10) NOT NULL,
  `monthly` int(10) NOT NULL,
  `weekly` int(10) NOT NULL,
  `notes` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(10) UNSIGNED NOT NULL,
  `cat_title` text NOT NULL,
  `cat_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_title`, `cat_desc`) VALUES
(1, 'Journal', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab voluptates repellendus facilis impedit consectetur commodi enim architecto, perspiciatis ipsum, cum magnam ad voluptas temporibus hic, aspernatur sunt facere eum natus!\r\n'),
(2, 'Sticker', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab voluptates, cum magnam ad voluptas temporibus repellendus facilis impedit consectetur commodi enim architecto, perspiciatis ipsum, hic, aspernatur sunt facere eum natus!\r\n'),
(3, 'Others', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `due_amount` int(10) NOT NULL,
  `invoice_no` varchar(200) NOT NULL,
  `qty` int(10) NOT NULL,
  `annual` int(10) NOT NULL,
  `monthly` int(10) NOT NULL,
  `weekly` int(10) NOT NULL,
  `notes` int(10) NOT NULL,
  `order_date` date NOT NULL,
  `order_status` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `orders`
--

INSERT INTO `orders` (`order_id`, `customer_id`, `due_amount`, `invoice_no`, `qty`, `annual`, `monthly`, `weekly`, `notes`, `order_date`, `order_status`) VALUES
(6, 4, 60, '1754990824', 1, 1, 1, 1, 0, '2022-06-08', 'pending'),
(7, 4, 75, '117565111', 1, 1, 1, 1, 0, '2022-06-08', 'pending'),
(8, 4, 60, '2031282619', 1, 1, 1, 1, 0, '2022-06-08', 'pending'),
(9, 4, 60, '1195991133', 1, 1, 1, 1, 0, '2022-06-08', 'pending');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `payments`
--

CREATE TABLE `payments` (
  `payment_id` int(10) UNSIGNED NOT NULL,
  `invoice_no` int(10) NOT NULL,
  `amount` int(10) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `iban` varchar(255) NOT NULL,
  `bic` varchar(255) NOT NULL,
  `payment_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `pending_orders`
--

CREATE TABLE `pending_orders` (
  `order_id` int(10) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `invoice_id` int(10) NOT NULL,
  `product_id` varchar(200) NOT NULL,
  `qty` int(10) NOT NULL,
  `annual` int(10) NOT NULL,
  `monthly` int(10) NOT NULL,
  `weekly` int(10) NOT NULL,
  `notes` int(10) NOT NULL,
  `order_status` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `pending_orders`
--

INSERT INTO `pending_orders` (`order_id`, `customer_id`, `invoice_id`, `product_id`, `qty`, `annual`, `monthly`, `weekly`, `notes`, `order_status`) VALUES
(2, 4, 1195991133, '6', 1, 1, 1, 1, 0, 'pending');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `products`
--

CREATE TABLE `products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `p_cat_id` int(10) NOT NULL,
  `cat_id` int(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `product_title` text NOT NULL,
  `product_image` text NOT NULL,
  `product_price` int(10) NOT NULL,
  `product_keywords` text NOT NULL,
  `product_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `products`
--

INSERT INTO `products` (`product_id`, `p_cat_id`, `cat_id`, `date`, `product_title`, `product_image`, `product_price`, `product_keywords`, `product_desc`) VALUES
(6, 1, 1, '2022-06-04 08:30:25', 'Black Cover', 'product1.png', 60, 'diary', 'An elegant classic book with spaces for each day of the year in which one notes appointments or information.\n'),
(7, 1, 1, '2022-06-04 08:30:34', 'Blue Journal', 'product2.png', 60, 'diary', 'An elegant classic book with spaces for each day of the year in which one notes appointments or information.\r\n'),
(8, 1, 1, '2022-06-04 08:31:02', 'Grey Journal', 'product3.png', 60, 'diary', 'diary'),
(9, 1, 1, '2022-06-04 08:32:02', 'Yellow Journal', 'product4.png', 60, 'diary', 'Limited Summer Edition'),
(11, 2, 1, '2022-06-05 17:18:47', 'Sunrise Journal', 'gradient2.png', 70, 'diary', ''),
(12, 2, 1, '2022-06-05 17:19:12', 'Nightfall Journal', 'gradient1.png', 75, 'diary', ''),
(13, 4, 1, '2022-06-05 17:19:37', 'Skyline Journal', 'graphic1.png', 75, 'diary', ''),
(14, 4, 1, '2022-06-05 17:35:25', 'Splash Journal', 'graphic2.png', 75, 'diary', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `product_categories`
--

CREATE TABLE `product_categories` (
  `p_cat_id` int(10) UNSIGNED NOT NULL,
  `p_cat_title` text NOT NULL,
  `p_cat_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `product_categories`
--

INSERT INTO `product_categories` (`p_cat_id`, `p_cat_title`, `p_cat_desc`) VALUES
(1, 'Plain Journal', 'Our plain leather has an icon, designed by Lucia Smith.\r\nLightweight, flexible and hard-wearing, just like its namesake, this calf leather beautifully combines form and function.'),
(2, 'Gradient Journal', 'Verschiedene Farbgradienten verzieren das Cover.'),
(4, 'Graphic Journal', 'Journals with graphic art on them.');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `slideshow`
--

CREATE TABLE `slideshow` (
  `slide_id` int(10) UNSIGNED NOT NULL,
  `slide_name` varchar(255) NOT NULL,
  `slide_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `slideshow`
--

INSERT INTO `slideshow` (`slide_id`, `slide_name`, `slide_image`) VALUES
(1, 'Slide Number 1', 'slide1.jpg'),
(2, 'Slide Number 2', 'slide2.jpg'),
(3, 'Slide Number 3', 'slide3.jpg');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `telnumber` varchar(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `profilepicture` varchar(255) NOT NULL,
  `user_ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`user_id`, `username`, `firstname`, `lastname`, `email`, `pwd`, `telnumber`, `adress`, `country`, `state`, `zip`, `profilepicture`, `user_ip`) VALUES
(1, 'Vornam', 'Nachname', 'p@p.com', 'root', '$2y$10$rEgCzXGCm82v/SnVMQ/C1.tS/rCEqspfwfo/3OAORJ4TvneJCWbgW', '066', 'pstraße', 'pest', 'test', '123', 'cat.jpg', '::1'),
(4, 'admin', 'Sandra', 'Thonakkara', 'admin@sandra.com', '$2y$10$Awg.8v5qZ3.8RA4ndX6kfeM5ySexxt13NzQO.D/KAunS.jteCgCSu', '0699150606060', 'Teststrasse', 'Testland', 'Teststadt', '1000', 'cat2.PNG', '::1'),
(5, 'asdf', 'Ali', 'Asdf', 'ali.asdf@gmail.com', '$2y$10$AmfB/LCZmwmr0jnRcACTNexfuWfIuKae51old33mcK6P9FOD2yMU.', '0680123123', 'alistrasse', 'aliland', 'alistadt', 'A123', '', '::1'),
(6, 'ehugo', 'Ernst', 'Hugo', 'hugo@e.at', '$2y$10$PWXvTTz.Ei9LenlAo4KCEOaHZCSCfl5m2IEhPJkX2iyozvfhYj042', '01283593656', 'Hugonierstraße', 'Hugonien', 'Gon', '1', 'hugo.jpg', '::1'),
(7, 'babs', 'Barbara', 'Bauer', 'Babs@bauer.at', '$2y$10$ei7lKxgtUpdI3lG7HuQs/OAPdcE2HGEWNG/4EX.7QFgyDyLDpIO4e', '123', 'Babsstrasse', 'Barbaraba', 'Bart', '123', 'babs.jpg', '::1');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`p_id`);

--
-- Indizes für die Tabelle `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indizes für die Tabelle `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indizes für die Tabelle `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indizes für die Tabelle `pending_orders`
--
ALTER TABLE `pending_orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indizes für die Tabelle `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indizes für die Tabelle `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`p_cat_id`);

--
-- Indizes für die Tabelle `slideshow`
--
ALTER TABLE `slideshow`
  ADD PRIMARY KEY (`slide_id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT für Tabelle `payments`
--
ALTER TABLE `payments`
  MODIFY `payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `pending_orders`
--
ALTER TABLE `pending_orders`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT für Tabelle `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `p_cat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `slideshow`
--
ALTER TABLE `slideshow`
  MODIFY `slide_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
