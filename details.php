<?php
    include("includes/header.php");
?>

        <!-- Start Main Shop -->
        <div class="container pt-3">
            <div class="col-md-12">
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Shop</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="shop.php?p_cat<?php echo $p_cat_id; ?>"><?php echo $p_cat_title; ?></a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <?php
                                echo "$product_title";
                            ?>
                        </li>

                    </ol>
                </nav>
            </div>
            <div class="row">
                <div class="col-3">
                    <?php
                    include("includes/sidebar.php");
                    ?>
                </div>
                <div class="col-9">
                    <?php
                         if (isset($_GET["error"])){
                            if($_GET["error"] == "stmtfailed"){
                                echo '<span style="color:#FF0000;text-align:center;">Something went wrong, try again</span>';
                            } else if($_GET["error"] == "none") {
                                echo '<span style="color:#32CD32;text-align:center; font-weight:bold;">Added To Cart!</span>';
                                }
                        }
                    ?>
                    <div class="row">
                        <div class="col-6">
                            <div class="productimage">
                                <img src="admin/product_images/<?php echo "$product_image" ?>" widht="300" height="300">
                            </div>
                        </div>
                        
                        <div class="col-6">
                            <h4 class="d-flex justify-content-between align-items-center mb-3">
                                <span class="text-primary"><?php echo "$product_title" ?></span>
                            </h4>
                            <!-- TODO: Not have all these journal details with other products such as stickers -->
                            <p>
                                <span class="badge bg-secondary px-2">
                                    Premium Leather <i class="bi bi-patch-check"></i>
                                </span>
                            </p>

                            <?php add_cart() ?>

                            <form action="details.php?add_cart=<?php echo $product_id; ?>" method="post">
                                <ul class="list-group list-group-flush mb-3">
                                    <li class="list-group-item d-flex justify-content-between lh-sm">
                                        <div>
                                        <h6 class="my-0">Jahresansicht</h6>
                                        </div>
                                        <!-- hidden input since not checked boxes do not get posted -->
                                        <input type="hidden" name="year" value="0">
                                        <input class="form-check-input" name="year" value="1" type="checkbox" checked>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between lh-sm">
                                        <div>
                                        <h6 class="my-0">Monatsansicht</h6>
                                        </div>
                                        <input type="hidden" name="month" value="0">
                                        <input class="form-check-input" name="month" value="1" type="checkbox" checked>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between lh-sm">
                                        <div>
                                            <h6 class="my-0">Wochenansicht</h6>
                                        </div>
                                        <input type="hidden" name="weeks" value="1">
                                        <input class="form-check-input" name="weeks" value="1" type="checkbox" checked disabled>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between bg-light">
                                        <div class="text-primary">
                                        <h6 class="my-0">Notizen</h6>
                                        <small>Leere Notizen am Ende</small>
                                        </div>
                                        <input type="hidden" name="notes" value="0">
                                        <input class="form-check-input" name="notes" value="1" type="checkbox" chekced>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between lh-sm">
                                    <div>
                                    <h6 class="my-0">Menge </h6>
                                    </div>
                                        <select name="product_qty" class="form-control">
                                           <option>1</option>
                                           <option>2</option>
                                           <option>3</option>
                                           <option>4</option>
                                           <option>5</option>
                                        </select>
                                    </li>    
                                    <li class="list-group-item d-flex justify-content-between lh-sm">
                                        <button class="btn btn-primary bi bi-cart-plus"> Add to cart</button>
                                    </li>                             
                                </ul>
                            </form>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <a class="list-group-item list-group-item-action active" aria-current="true">
                            <div class="d-flex justify-content-between">
                            <h5 class="mb-1">Product Details</h5>
                            <p class="mb-1"><?php echo "$product_desc" ?></p>
                        </a>
                    </div>
                </div>   
            </div> 
        </div>
        <!-- End Shop -->

        <?php
            include("includes/footer.php");

        ?>

        <script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
    </body>
</html>