<?php
  // include_once ("includes/db.php");

  $session_email = $_SESSION['customer_email'];
  $select_customer = "SELECT * FROM users WHERE email='$session_email'";
  $run_customer = mysqli_query($conn, $select_customer);
  $row= mysqli_fetch_array($run_customer);
  $user_id = $row["user_id"];

?>

<h4 class="mb-3">Zahlungsmethoden</h4>
<div class="row row-cols-1 row-cols-md-2 g-4">
  <div class="col">
    <div class="card">
    <div class="card-header">
  <i class="bi bi-bank"></i> SEPA Lastschrift
  </div>
      <div class="card-body">
        <p class="card-text"> Gib deine Kontodaten an in deinen Aufträgen und wir buchen dir das Geld in 3-5 Werktagen ab!</p>
        <!-- <ul>
        <li><strong>Empfänger:</strong> CYJ Inc.</li>
        <li><strong>IBAN:</strong> AT12 3450 6538 6823</li>
        <li><strong>BIC:</strong> BKAUATWW</li>
        </ul> -->
        <div class="d-grid gap-2">
            <a href="orders.php?user_id=<?php echo $user_id; ?>"ton class="btn btn-primary" type="button">
              Weiter
            </a>
        </div>
    </div>
    </div>
  </div>
  <div class="col">
    <div class="card">
    <div class="card-header">
    <i class="bi bi-paypal"></i> Paypal
  </div>
      <div class="card-body">
        <p class="card-text">Wähle beim Bezahlprozess Paypal aus und du wirst automatisch auf die Paypal Seite weitergeleitet.</p>
        Einfach und schnell.
        <div class="d-grid gap-2">
        <a class="btn btn-primary disabled" aria-disabled="true" role="button" data-bs-toggle="button">Bald verfügbar</a>
        </div>
    </div>
    </div>
  </div>
</div>